
public class ArvoreAVL<T extends Comparable<T>> extends ArvoreBinaria<T> {

    public ArvoreAVL(boolean raiz) {
	super(raiz);
    }
    
    /**
     * Definições:
     * Pai pela esquerda - pai o qual eu sou filho pela direita
     * Pai pela direita - pai o qual eu sou filho pela esquerda
     * 
     * Fator de balanceamento : he - hd
     * 
     * Rotações:
     * Simples:
     *     RR - Rotação à esquerda - Pai PELA esquerda se torna filho à esquerda
     *     LL - Rotação à direita - Pai PELA direita se torna filho à direita
     * 
     * Dupla:
     *     LR - Rotação à esquerda e depois à direita - RR/LL 
     *     RL - Rotação à direita e depois à esquerda - LL/RR 
     * 
     * 
     * 
     * Situações de desbalanceamento:
     * 
     * 
     * 	Desbalanceada pela direita duas vezes - Fator de balanceamento -2, e meu filho
     * 	à direita também tem fator de balanceamento negativo. Ou seja, lado direito está mais pesado
     * 	duas vezes
     * 	----Rotação RR
     * 
     *
     * Desbalanceada pela esquerada duas vezes - Fator de balanceamento +2, e meu filho à esquerda
     * também tem fator de balanceamento positivo. Ou seja, lado esquerdo está mais pesado duas vezes
     * ----Rotação LL
     * 
     * 
     * Desbalanceada pela esquerda e depois pela direita - Fator de balanceamento +2, e meu filho à esquerda
     * tem fator de balanceamento negativo. Ou seja, lado esquerdo está mais pesado, e ao descer pela esquerda, 
     * lado direito está mais pesado
     * ----Rotação LR
     * 
     * Desbalanceada pela direita e depois pela esquerda - Fator de balanceamento -2, e meu filho à esquerda
     * tem fator de balanceamento positivo. Ou seja, lado direito está mais pesado, e ao descer pela direita, 
     * lado esquerdo está mais pesado
     * ----Rotação RL
     */

}
