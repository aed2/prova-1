
public class Main {
    public static void main(String args[]) {
	testeBinaria();
    }
    
    static void testeBinaria() {

	ArvoreBinaria<Integer> arvore = new ArvoreBinaria<Integer>(true);

	arvore.inserir(6);
	arvore.inserir(10);
	arvore.inserir(2);
	arvore.inserir(20);
	arvore.inserir(-50);
	arvore.inserir(100);
	arvore.inserir(15);
	arvore.inserir(7);
	arvore.inserir(-100);
	arvore.inserir(-40);
	

	
	System.out.println("\n\nImpressão: \n");
	arvore.imprimir("");
	
	System.out.println("\n\n");
	

	arvore.remover(20);
	System.out.println("Impressão: \n");
	arvore.imprimir("");
	

	System.out.println();
	System.out.println();

	System.out.println("Impressão: \n");
	arvore.imprimir("");
	

	System.out.println("\n\n");
	arvore.erdRec();
	System.out.println();
	arvore.erdIterativo();
	
	System.out.println("\n");
	System.out.println(arvore.altura());
	
	

	System.out.println("\n\n\n");

	arvore.inserir(14);
	arvore.inserir(13);
	arvore.inserir(12);
	arvore.inserir(11);
	arvore.inserir(20);
	arvore.inserir(30);
	arvore.inserir(40);
	arvore.inserir(90);
	System.out.println("Impressão: \n");
	arvore.imprimir("");
	arvore.erdRec();
	

	System.out.println("\n\n");
	ArvoreBinaria<Integer> a = arvore.busca(100);
	System.out.println("\nBusca: " + a);

	System.out.println("Predecessor: " + a.predecessor());

	
	a = arvore.busca(10);
	System.out.println("\n\n\nBusca: " + a);
	System.out.println("Sucessor: " + a.sucessor());
	
	a = arvore.busca(-40);
	System.out.println("\n\n\nBusca: " + a);
	System.out.println("Sucessor: " + a.sucessor());

	System.out.println("\n\nNúmero de elementos: " +  arvore.size + "\n\n");

	arvore.remover(2);
	arvore.remover(6);
	
	arvore.imprimir("");
	System.out.println();
	
	
	
	arvore.erdIterativo();
	arvore.reconstruir();
	
	System.out.println("\n\nÁrvore reconstruída: ");

	arvore.imprimir("");
	System.out.println();
	

	System.out.println("\n\nNúmero de elementos: " +  arvore.size + "\n\n");
	arvore.erdIterativo();
	
	arvore.remover(13);

	arvore.imprimir("");
	System.out.println();
    }
}
