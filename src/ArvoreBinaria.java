import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class ArvoreBinaria<T extends Comparable<T>> extends Arvore<T> { 
    int size = 1;
    boolean raiz;
    
    public ArvoreBinaria(T initValue, boolean raiz) {
	super(initValue);
	this.raiz = raiz;
    }
    
    public ArvoreBinaria(boolean raiz) {
	super();
	this.raiz = raiz;
    }


    ArvoreBinaria<T> esq, dir;
    
    
    int altura() {
	int aEsq, aDir;
	aEsq = aDir = 0;
	

	if(esq != null) {
	    aEsq = esq.altura();
	}

	if(dir != null) {
	    aDir = dir.altura();
	}
	
	if(aEsq > aDir) {
	    return aEsq + 1;
	} else {
	    return aDir + 1;
	}
    }
    
    ArvoreBinaria<T> busca(Comparable o) {
	ArvoreBinaria<T> atual, anterior;
	
	ArvoreBinaria<T> resultado = null;
	atual = anterior = this;
	
	while(atual != null) {
	    int comp = o.compareTo(atual.chave);
        	    
	    if(o.equals(atual.chave)) {
		resultado = atual;
		atual = null;
	    } else {
        	    if(comp < 0) {
        		atual = atual.esq;
        	    } else if(comp > 0) {
        		atual = atual.dir;
        	    } 
	    }
	}
	
	return resultado;
    }

    public void preorderRec() {
      System.out.print(" " + super.chave.toString());

      if(this.esq != null)
        this.esq.preorderRec();


      if(this.dir != null)
        this.dir.preorderRec();
    }


    public void inorderRec() {

      if(this.esq != null)
        this.esq.inorderRec();

      System.out.print(" " + this.chave.toString());

      if(this.dir != null)
        this.dir.inorderRec();
    }


    public void posorderRec() {

      if(this.esq != null)
        this.esq.posorderRec();


      if(this.dir != null)
        this.dir.posorderRec();
        System.out.print(" " + this.chave.toString());
    }
    
    
    void imprimir(String prepend) {
	System.out.println(prepend + "(" + this.chave.toString() + ")");
	
	boolean bothNull = this.esq == null && this.dir == null;
	
	if(this.esq != null)
		this.esq.imprimir(prepend + "----");	
	else if(!bothNull)
	    System.out.println(prepend + "---- (null)");
	
	if(this.dir != null)
		this.dir.imprimir(prepend + "----");
	else if(!bothNull)
	    System.out.println(prepend + "---- (null)");
    }
    
    void erdRec() {
	if(this.esq != null)
	    this.esq.erdRec();
	
	System.out.print(this.chave.toString() + " ");
	
	if(this.dir != null)
	    this.dir.erdRec();
	
    }
    

    List<T> erdIterativo() {
	return erdIterativo(true);
    }
    
    List<T> erdIterativo(boolean imprimir) {
	List<T> lista = new ArrayList<T>(this.size);
	
	Stack<ArvoreBinaria<T>> pilha = new Stack<ArvoreBinaria<T>>();
	
	ArvoreBinaria<T> atual = this;
	
	while(atual != null) {
	    while(atual != null) {
    	
        	if(atual.dir != null)
        	    pilha.add(atual.dir);
        	    
        	pilha.add(atual);
        	
        	atual = atual.esq;
	    }
	    
	    atual = pilha.pop();
	    
	    while(!pilha.isEmpty() && atual.dir == null) {
		if(imprimir)
		    System.out.print(atual.toString() + " ");
		
		lista.add(atual.chave);
		
		atual = pilha.pop();
	    }
	    
	    if(imprimir)
		System.out.print(atual.toString() + " ");
	    lista.add(atual.chave);
		    
	    
	    if(pilha.isEmpty())
		atual = null;
	    else
		atual = pilha.pop();
	}
	
	if(imprimir)
	    System.out.println();

	return lista;
	
    }

    ArvoreBinaria<T> predecessor() {
	if(this.esq != null) {
	    ArvoreBinaria<T> p = (ArvoreBinaria<T>) this.esq;
	    
	    while(p.dir != null) {
		p = p.dir;
	    }
	    
	    return p;
	} else {
	    ArvoreBinaria<T> p = (ArvoreBinaria<T>) this;
	    
	    while(p != p.getPai().dir) {
		p = p.getPai();
	    }
	    
	    return p.getPai();
	}
	
    }
    ArvoreBinaria<T> sucessor() {
	if(this.dir != null) {
	    ArvoreBinaria<T> p = (ArvoreBinaria<T>) this.dir;
	    
	    while(p.esq != null) {
		p = p.esq;
	    }
	    
	    return p;
	} else {
	    ArvoreBinaria<T> p = (ArvoreBinaria<T>) this;
	    
	    while(p != p.getPai().esq) {
		p = p.getPai();
	    }
	    
	    return p.getPai();
	}
	
    }

    void setEsq(ArvoreBinaria<T> filho) {
	this.esq = filho;
	
	if(filho != null)
	    filho.pai = this;
    }
    
    void setDir(ArvoreBinaria<T> filho) {
	
	this.dir = filho;
	
	if(filho != null)
	    filho.pai = this;
    }
    
    ArvoreBinaria<T> getPai() {
	return (ArvoreBinaria<T>) this.pai;
    }
    

    void inserir(ArvoreBinaria<T> filho) {
	if(this.raiz)
	    size++;
	
	if(filho != null) {
    	    ArvoreBinaria<T> anterior = this, atual = this;
        	    
            while(atual != null) {
                anterior = atual;
                if(filho.chave.compareTo(atual.chave) < 0) {
                	atual = atual.esq;
                } else {
                	atual = atual.dir;
                }
            }
                
            if(filho.chave.compareTo(anterior.chave) < 0)
                anterior.setEsq(filho);
            else
                anterior.setDir(filho);
	}
	  
    }
    
    void inserir(T chave) {
	if(chave != null) {    
	    if(!this.temChave) {
	    	this.setChave(chave);
	    	this.chave = chave;
	    } else {
    	        ArvoreBinaria<T> filho = new ArvoreBinaria<T>(chave, false);
    	        this.inserir(filho);
	    }           
    	}
    }
    
    void remover(T chave) {

	if(this.raiz)
	    size--;
	
    	ArvoreBinaria<T> anterior = null, atual = this;
    	
    	//Localizar o pai
    	//Não é mais necessário, já que o atributo pai foi definido
    	while(atual != null) {
    	    if(atual.chave.equals(chave)) {
    		atual = null;
    	    } else {
    		anterior = atual;
                if(chave.compareTo(atual.chave) < 0) {
            	    atual = atual.esq;
                } else {
            	    atual = atual.dir;
                }
    	    }
    	}

	//anterior == null - não tem pai - nó a remover é a raiz
    	if(anterior == null) {
    	    if(this.esq != null) { //Nó a esquerda é a nova raiz
    		ArvoreBinaria<T> antigoEsquerda = this.esq;
        	this.chave = antigoEsquerda.chave;
        	
        	this.setEsq(antigoEsquerda.esq);
        	
        	if(this.dir == null)
        	    this.setDir(antigoEsquerda.dir);
        	else
        	    this.dir.inserir(antigoEsquerda.dir);
    	    } else {
    		ArvoreBinaria<T> antigoDireita = this.dir;
        	this.chave = antigoDireita.chave;
        	
        	this.setDir(antigoDireita.dir);
        	
        	if(this.esq == null)
        	    this.setEsq(antigoDireita.esq);
        	else
        	    this.esq.inserir(antigoDireita.esq);
    		
    	    }
    	} else {
        	if(anterior.esq != null && chave.equals(anterior.esq.chave)) {
        	    if(anterior.esq.esq != null) {
        		anterior.esq.esq.inserir(anterior.esq.dir);
        		//anterior.esq.esq.setDir(anterior.esq.dir);
        	    	anterior.setEsq(anterior.esq.esq);
        		//anterior.esq = anterior.esq.esq;
        	    } else {
        		anterior.setEsq(anterior.esq.dir);
        		//anterior.esq = anterior.esq.dir;
        	    }
        	} else if(anterior.dir != null && chave.equals(anterior.dir.chave)) {
        	    
        	    if(anterior.dir.dir != null) {
        		anterior.dir.dir.inserir(anterior.dir.esq);
        		//anterior.dir.dir.setEsq(anterior.dir.esq);
        		anterior.setDir(anterior.dir.dir);
        	    	//anterior.dir.dir.esq = anterior.dir.esq;
        	    	//anterior.dir = anterior.dir.dir;
        	    } else {
        		anterior.setDir(anterior.dir.esq);
        		//anterior.dir = anterior.dir.esq;
        	    }
        	}
    	}
    	
    }
    
    
    /**
     * Refazer completamente a árvore, procurando colocar como raiz o valor intermediário do conjunto,
     * de forma que se busque ter a mesma quantidade de nós em cada um dos lados.
     * 
     * Complexidade:
     * 
     * n - varredura ERD
     * n - construir a árvore (varrer toda a lista estrategiacamente)
     * 
     * O(2n)
     */
    void reconstruir() {
	List<T> lista = this.erdIterativo(false);
	
	this.construir(lista, 0, lista.size() - 1);
    }
    
    void construir(List<T> lista, int inicio, int fim) {
	int pivotIndex = inicio + (fim - inicio + 1) / 2;
	T pivot = lista.get(pivotIndex);
	
	this.setChave(pivot);
	

	ArvoreBinaria<T> esq, dir;
	esq = new ArvoreBinaria<T>(false);
	dir = new ArvoreBinaria<T>(false);

	
    	if(pivotIndex - 1 >= inicio && inicio != fim) {
    	    this.setEsq(esq);
    	    esq.construir(lista, inicio, pivotIndex - 1);
    	}
    	   
    	if( pivotIndex + 1 <= fim && inicio != fim) {
    	    this.setDir(dir);
    	    dir.construir(lista, pivotIndex + 1, fim);
    	}

    }
    
    
    
}
