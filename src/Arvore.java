
public abstract class Arvore<T extends Comparable<T>> {
    Arvore<T> pai;
     
    T chave;
    boolean temChave;
    
    
    public Arvore(T initValue) {
	this.temChave = true;
	this.chave = initValue;
    }    
    
    public Arvore() {
    }
    
    public String toString() {
	return chave.toString();
    }

    public void setChave(T chave) {
	this.chave = chave;
	this.temChave = true;
    }
    
}
